import routerx from "express-promise-router";

import usuarioController from "../controllers/usuarioController";
import auth from "../middlewares/auth";
import { getUser, autenticarLdap } from "../middlewares/ldap";

const router = routerx();

router.get("/:id", auth.verifyAdmin, usuarioController.findById);
router.get("/", auth.verifyAdmin, usuarioController.list);
router.post("/", auth.verifyAdmin, usuarioController.add);

router.put("/:id", auth.verifyAdmin, usuarioController.update);
router.post("/login", getUser, /*autenticarLdap,*/ usuarioController.login);

export default router;
