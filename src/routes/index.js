import routerx from "express-promise-router";

import usuarioRouter from "./usuarioRouter";

import pessoasRouter from "./pessoasRouter";

const router = routerx();

router.use("/usuarios", usuarioRouter);

router.use("/pessoas", pessoasRouter);

export { router };
