import routerx from "express-promise-router";
import pessoasController from "../controllers/pessoasController";
const router = routerx();
router.get("/", pessoasController.list);
router.post("/", pessoasController.add);
router.get("/:id", pessoasController.findById);
router.put("/:id", pessoasController.update);
export default router;
