'use strict';

module.exports = {

    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('pessoas', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },

            nome: {
                allowNull: false,
                type: Sequelize.STRING
            },

            observacao: {
                allowNull: false,
                type: Sequelize.STRING(5000)
            },

            endereco: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },
			email: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },
			telefone: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },

            created_at: {
                allowNull: false,
                defaultValue: Sequelize.fn('NOW'),
                type: Sequelize.DATE
            },

            updated_at: {
                allowNull: false,
                defaultValue: Sequelize.fn('NOW'),
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('pessoas');
    }
};