'use strict';

module.exports = (sequelize, DataTypes) => {
    const Usuario = sequelize.define('Usuario', {
        nome: DataTypes.STRING,
        usuario: DataTypes.STRING,
        
        
    }, {
        freezeTableName: true,
        tableName: 'usuarios'
    });

    

    return Usuario;
};