'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('pessoas', [{
            id: 1,
            nome: 'jose',
            observacao: 'Disciplina o funcionamento da seção de Arquivo e Depósito Judicial e dá outras providências.',
            endereco: 'd6fffb808527158f7ee199c94166149b',
			email:'ja@c.com',
            telefone: '7897987',
            
            created_at: '2006-07-24 00:00:00.000',
            updated_at: '2006-07-24 00:00:00.000'
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('pessoas', null, {});
    }
};
