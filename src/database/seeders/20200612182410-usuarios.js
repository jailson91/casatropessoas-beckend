'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('usuarios', [{
            id: 1,
            nome: 'Ricardo Correia de Miranda Henriques',
            usuario: 'ricardo',

            created_at: Sequelize.fn('NOW'),
            updated_at: Sequelize.fn('NOW')
        }, {
            id: 2,
            nome: 'Luiz Linderman de Queiroz Medeiros Sobrinho',
            usuario: 'linderman',
           
            created_at: Sequelize.fn('NOW'),
            updated_at: Sequelize.fn('NOW')
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('usuarios', null, {});
    }
};
